Feature Iterator is a plugin for QGis. Check it's homepage at http://plugins.qgis.org/plugins/FeatureIterator/

This plugin's purpose is to iterate over a vector layer's features, having the possibility to remove each feature during a step-by-step iteration. One can also define a pair attribute-value that will filter the features to be iterated. During iteration, viewport will be panned to where the feature is, using the same zoom level defined by user.

Centro de Informação Geoespacial do Exército (www.igeoe.pt)

Portugal